import * as React from "react"
import { Card, Select, Icon, InputNumber } from "antd"
import {StatuMatrix} from 'types'
import "./index.css"
import {headerView, widthRatio, HypoTextView, TitleView, THR} from 'const'

const { Option } = Select

interface Props{
    changeTHR:(thr:number|undefined)=>void
}



export default class Header extends React.Component<Props, {}> {
    render() {
        let {width, height} = headerView

        let modelSelector = <Select defaultValue="CNN" >
            <Option value="CNN">CNN</Option>
        </Select>

        let pSelector = <InputNumber min={0} max={0.5} step={0.01}  defaultValue={THR} onChange={this.props.changeTHR}/>

        
        let reject = false
        let checkIcon = <Icon type={`${reject?"close":"check"}-circle`} style={{fontSize: "1.6em"}}/>

        return <div className='header' style={{width, height, position:"absolute", top: HypoTextView.height-HypoTextView.margin.top+TitleView.height, overflow:"auto"}}>

            


            <div className='hypo component' style={{width: (width||0)*widthRatio.hypo, height}}>
                <h3>Hypothesis</h3>
                {/* <Card size="small" title={<span>Model Hypothesis</span>} >
                    The model <b>M</b>  has learned the feature <b>F</b> from the original dataset <b>D</b> 
                    
                </Card>

                <Card size="small" title={<span>Feature Hypothesis</span>} >
                    The Feature <b>F</b> is helpful for the prediction.
                    
                </Card> */}
            </div>

            <div className='p_value component' style={{width: (width||0)*widthRatio.p, height}}>
                <h3>P Values
                { pSelector}
                </h3> 
            </div>

            <div className='Model component' style={{width: (width||0)*widthRatio.model, height}}>
                <h3>Models
                { modelSelector} 
                </h3> 
            </div>

            

            
        </div>
    }
}
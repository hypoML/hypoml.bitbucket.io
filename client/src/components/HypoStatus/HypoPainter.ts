import * as d3 from 'd3'
import { Painter } from 'components/Painter'
import {ViewParams, StatuMatrix, STATUS} from 'types';
import { HYPO, questionIcon, rejectIcon, supportIcon, conditionIcon, dummyIcon, curveMaker, ANALYSIS} from 'helper';

export interface Data {
    status: StatuMatrix
}
export interface Params extends ViewParams, Data {
 }


export default class  PGlyphPainter implements Painter<Data, ViewParams>{
    private params: Params; r: number = 8
    constructor(params: Params) {
        this.params = params
    }

    update(newParams: Partial<Params>) {
        this.params = { ...this.params, ...newParams }
        return this
    }

    render(selector:d3.Selection<SVGGElement, Data, any, any>) {
        let {status, height, width, margin} = this.params
        // height = height - margin.top - margin.bottom
        width = width - margin.left - margin.right


        // let yScale = d3.scaleLinear()
        // .domain([0, status.length+1])
        // .range([0, height])

        let xBand = width/(HYPO.length), yBand = height/(ANALYSIS.length-1)

        let hypoGroups = selector.selectAll('g.hypo')
        .data(status)
        .join(
            enter => enter.append('g')
            .attr('class', 'hypo')
            .attr('transform',(_,i)=>`translate(${xBand *i}, ${yBand*0.5})`),

            update =>update
            .attr('class', 'hypo')
            .attr('transform',(_,i)=>`translate(${xBand *i}, ${yBand*0.5})`)
        )


        

        // draw icon
        hypoGroups.selectAll('path.statusIcon')
        .data(d=>d)
        .join(
            enter => enter.append('path')
            .attr('class', 'statusIcon')
            .attr('d', d=>getIconPath(d))
            .attr('fill', d=>getIconColor(d))
            .attr('stroke', d=>(d.status==undefined)?'gray':'none')
            .attr('stroke-width', '2')
            .attr('transform', (_,j)=>`translate(${0}, ${yBand *j}) `),

            update=>update
            .attr('d', d=>getIconPath(d))
            .attr('fill', d=>getIconColor(d))
            .attr('stroke', d=>(d.status==undefined)?'gray':'none')
            .attr('stroke-width', '2')
            .attr('transform', (_,j)=>`translate(${0}, ${yBand*j}) `),

            exit => exit.remove()
        )

        // // draw dependent hypo
        // hypoGroups.selectAll('path.ifHypo')
        // .data((d, i)=>d.map((anly)=>{
        //     return {anly, hypoIdx:i}
        // }))
        // .join(
        //     enter=>enter.append('path')
        //     .attr('class', 'ifHypo')
        //     .attr('d', (d,j)=>{
        //         let {anly, hypoIdx} = d
        //         // return anly.if?`M${xBand*status[0].length} ${yBand*(anly.if-1-hypoIdx+0.5)+xBand*0.5} L${xBand*(j+0.5)} ${yBand*(0.5)}`:''
        //         let dependHypoPos:[number, number] = [xBand*((anly.if||0)-1-hypoIdx), yBand*(status[0].length-0.5)],
        //         currentAnalyPos:[number, number] = [0, yBand*(j+0.5)]
        //         // if ((anly.if||0)<hypoIdx){
        //         //     [dependHypoPos, currentAnalyPos] = [currentAnalyPos, dependHypoPos]
        //         // }

        //         if (! anly.if) return ''
        //         else if (anly.if>hypoIdx) return curveMaker(dependHypoPos, currentAnalyPos) 
        //         else return curveMaker(currentAnalyPos, dependHypoPos)
        //     }),

        //     update=>update
        //     .attr('d', (d,j)=>{
        //         let {anly, hypoIdx} = d
        //         // return anly.if?`M${xBand*status[0].length} ${yBand*(anly.if-1-hypoIdx+0.5)+xBand*0.5} L${xBand*(j+0.5)} ${yBand*(0.5)}`:''
        //         let dependHypoPos:[number, number] = [xBand*((anly.if||0)-1-hypoIdx), yBand*(status[0].length-0.5)],
        //         currentAnalyPos:[number, number] = [0, yBand*(j+0.5)]
        //         // if ((anly.if||0)<hypoIdx){
        //         //     [dependHypoPos, currentAnalyPos] = [currentAnalyPos, dependHypoPos]
        //         // }

        //         if (! anly.if) return ''
        //         else if (anly.if>hypoIdx) return curveMaker(dependHypoPos, currentAnalyPos) 
        //         else return curveMaker(currentAnalyPos, dependHypoPos)
        //     }),
        //     exit => exit.remove()

        // )
        // .attr('stroke', 'black')
        // .attr('fill', 'none')


        return this
    }

}

function getIconPath(d: StatuMatrix[0][0]){
    switch (d.status){
        case 1: return supportIcon 
        case 0: return questionIcon 
        case -1: return rejectIcon
        case 'if': return conditionIcon
        default: return dummyIcon
    }
}

function getIconColor(d: StatuMatrix[0][0]){
    switch (d.status){
        case 1: return 'cornflowerblue'; 
        case 0: return 'gray';
        case -1: return 'orange';
        case 'if': return 'black'
        default: return 'none';
    }
}

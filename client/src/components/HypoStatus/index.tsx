import * as React from "react"
import * as d3 from 'd3'
import { CompareData, StatuMatrix } from 'types'
import { HYPO, ANALYSIS } from 'helper'
import { hypoMatrixView, HypoTextView} from 'const'
import "./index.css"
import HypoPainter, { Params } from './HypoPainter'



interface Props {
    status: StatuMatrix
}
// const THR = 0.02

export default class Hypothesis extends React.Component<Props, {}> {
    private painter: HypoPainter
    constructor(props: any) {
        super(props)
        let { height, margin, width } = hypoMatrixView
        let { status} = this.props

        
        let params = {
            height,
            width,
            margin,
            status
        }
        this.painter = new HypoPainter(params)
    }
    painterUpdate(newParams: Partial<Params>) {
        this.painter.update(newParams)
            .render(d3.select('g.hypos'))
    }
    componentWillUpdate(nextProps: Props) {
        let params = { status: nextProps.status }
        this.painterUpdate(params)
        return false
    }
    componentDidMount() {
        this.painter.render(d3.select('g.hypos'))
    }
    render() {
        let { margin} = hypoMatrixView



        return <g transform={`translate(${margin.left}, ${HypoTextView.height})`} className='hypos' />
    }
}
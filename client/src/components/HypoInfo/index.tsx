import * as React from "react"
import * as d3 from 'd3'
import { StatuMatrix } from 'types'
import {  HYPO } from 'helper'
import { HypoTextView } from 'const'



interface Props {
    status: StatuMatrix
}


export default class Hypothesis extends React.Component<Props, {}> {

    textColor(status:number){
        // if (status==0) return 'grey';
        // else if(status>0) return 'cornflowerblue';
        // else if (status<0) return 'orange';

        if (status==0) return '#7a7a7a';
        else if(status>0) return '#000';
        else if (status<0) return '#c0c0c0';
    }

    render() {
        let { height, width, margin } = HypoTextView
        let {status} = this.props

        let hypoStatus = status.map(anlyList=>{
            return anlyList.filter(d=>typeof(d.status)=='number').reduce((acc, curr)=>acc+ (curr.status as number), 0)
        })

        width = width - margin.left - margin.right
        height = height - margin.top - margin.bottom

        let xScale = d3
            .scaleLinear()
            .domain([0, HYPO.length])
            .range([0, width])


        let hypoInfo = HYPO.map((hypo,i)=>{
            return <g className='hypoInfo' transform={`translate(${xScale(i)}, ${height})`}>
                <text className='hypoNumber' fontWeight='bold' fontSize={18} fill={this.textColor(hypoStatus[i])}>
                    {`H${i+1}`}
                </text> 
                <text className='hypoText' transform={`translate(10, -20) rotate(-50)`} fontSize={16} fill={this.textColor(hypoStatus[i])}>
                    {hypo.info.slice(0, hypo.info.length/2+1)}
                </text>   
                <text className='hypoText' transform={`translate(30, -20) rotate(-50)`} fontSize={16} fill={this.textColor(hypoStatus[i])}>
                    {hypo.info.slice(hypo.info.length/2+1)}
                </text>        
            </g>
        })

       
        return <g
            className='hypoInfos'
            transform={`translate(${margin.left}, ${margin.top})`}
        >
           {hypoInfo}
        </g>
    }
}
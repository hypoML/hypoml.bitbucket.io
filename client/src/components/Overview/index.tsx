import React from 'react';
import Models from 'components/Models';
import HypoStatus from 'components/HypoStatus';
import HypoInfo from 'components/HypoInfo';
import {CompareData, Results, ModelData, Label, Metric, StatuMatrix} from 'types';
import {fetchJSON} from 'dataService'
import {LABELS, overView, THR} from 'const'
import {get95conf, pairedT, ANALYSIS, HYPO, MODELS} from 'helper';
import Header from 'components/Header';
import InfoPanel from 'components/InfoPanel';

interface States {
    modelData: ModelData,
    compareData: CompareData,
    filename:string,
    thr:number
}

const METRIC = 'precision'

export default class Overview extends React.Component<{}, States>{
    
    constructor(props:any){
        super(props)
        this.state={
            filename:'fashion_1.Rotating.json',
            modelData:[],
            compareData: [],
            thr:THR
        }
        this.changeResults=this.changeResults.bind(this)
        this.changeTHR = this.changeTHR.bind(this)
        
    }
    // **************
    // data transformers
    // ****************
    getModeldata(res: Results): ModelData {
        let modelData: ModelData = MODELS.map(name => {
            let re = res[name]
            let err = get95conf([...Array(10).keys()].map((d) => {
                let label = d.toString() as Label
                return re[label][METRIC]
            }))
            
            let avg = re['avg'][METRIC]
            return {
                name, avg, err,
                arr: LABELS.map(l => re[l][METRIC]),
                hasDetails: false
            }
        })

        return modelData

    }
    getCompareData(res: Results): CompareData {
        let compareData: CompareData = ANALYSIS.map(a=>{
            let pair = a.pair, modelA = pair[0], modelB = pair[1]
            let samplesA = LABELS.map(l => res[modelA][l][METRIC]),
                    samplesB = LABELS.map(l => res[modelB][l][METRIC])

                return ({
                    from: modelA,
                    to: modelB,
                    p: pairedT(samplesA, samplesB, 1),
                    dir: res[modelA].avg[METRIC]>res[modelB].avg[METRIC]?'+':'-'
                })
        })
        return compareData
    }
    calculateStatus(compareData: CompareData): StatuMatrix {
        let results: StatuMatrix = HYPO.map(d => ANALYSIS.map(d => {
            return { status: undefined, if: undefined }
        }))

        // set status to 0 if analysis and hypo is related
        ANALYSIS.forEach((anly, j)=>{
            [anly.large, anly.less].forEach(res=>{
                if(res.dependHypo){
                    [res.ifSupport, res.ifSupport, res.ifReject].forEach(conclu=>{
                        [conclu.support, conclu.reject].forEach((hypos)=>{
                            hypos.forEach(hypoIdx=>results[hypoIdx-1][j].status=0)
                        })
                    })
                }else{
                    [res.support, res.reject].forEach((hypos)=>{
                        if(hypos) hypos.forEach(hypoIdx=>results[hypoIdx-1][j].status=0)
                    })
                }
            })
        })


        

        compareData.forEach((comp, j) => {

            if (comp.p < this.state.thr) {
                let res: any
                // significant larger
                if (comp.dir == '+') {
                    res = ANALYSIS[j].large
                    let dependHypo = res.dependHypo
                    if (dependHypo) {
                        let dependHypoStatus: number = results[res.dependHypo - 1]
                            .filter(item => item.status != undefined)
                            .reduce((total: number, item: any) => total + item.status, 0)
                        if (dependHypoStatus > 0) {
                            res = res.ifSupport

                        } else if (dependHypoStatus == 0) {
                            res = res.ifQuestion
                        } else if (dependHypoStatus < 0) {
                            res = res.ifReject
                        }
                        // res.support.forEach((hyIdx: number) => {
                        //     results[hyIdx-1][j].if = dependHypo
                        // })
                        // res.reject.forEach((hyIdx: number) => {
                        //     results[hyIdx-1][j].if = dependHypo
                        // })
                    }
                } else {
                // significant smaller
                    res = ANALYSIS[j].less
                    let dependHypo = res.dependHypo
                    if (dependHypo) {
                        let dependHypoStatus: number = results[res.dependHypo - 1]
                            .filter(item => item.status != undefined)
                            .reduce((total: number, item: any) => total + item.status, 0)

                        if (dependHypoStatus > 0) {
                            res = res.ifSupport
                        } else if (dependHypoStatus == 0) {
                            res = res.ifQuestion
                        } else if (dependHypoStatus < 0) {
                            res = res.ifReject
                        }

                        // res.support.forEach((hyIdx: number) => {
                        //     results[hyIdx-1][j].if = dependHypo
                        // })

                        // res.reject.forEach((hyIdx: number) => {
                        //     results[hyIdx-1][j].if = dependHypo
                        // })
                    }
                }
                // console.info(i,j,ANALYSIS[j], res)

                res.support.forEach((hypoIdx: number) => {
                    results[hypoIdx - 1][j].status = 1
                })
                res.reject.forEach((hypoIdx: number) => {
                    results[hypoIdx - 1][j].status = -1
                })
            }
        })

        // add if status

        // compareData.forEach((comp, j) => {
            
        //     if (comp.dir == '+') {
        //         var dependHypo = ANALYSIS[j].large.dependHypo
        //         if(dependHypo){
        //             results[dependHypo-1][j].status='if'
        //         }
        //     }else{
        //         var dependHypo = ANALYSIS[j].less.dependHypo
        //         if(dependHypo){
        //             results[dependHypo-1][j].status='if'
        //         }
        //     }
        // })


        results[4][3].status='if'
        results[4][4].status='if'
        results[5][3].status='if'
        results[5][4].status='if'
        results[0][5].status='if'
        results[1][5].status='if'


        return results
    }
    async getResults(filename:string){
        let res = await fetchJSON('data/'+filename)
        let modelData = this.getModeldata(res)
        let compareData = this.getCompareData(res)
        this.setState({filename, modelData, compareData})
    }
    componentDidMount(){
        this.getResults(this.state.filename)
    }
    changeResults(filename:string){
        this.getResults(filename)
    }
    changeTHR(thr:number|undefined){
        if (typeof(thr)=='number') this.setState({thr})
    }
    render(){
        let {width, height, margin} = overView
        let {compareData} = this.state
        let status = this.calculateStatus(compareData)
        return <div>
            <InfoPanel changeResults={this.changeResults} filename={this.state.filename}/>
            <div style={{width, height}}>
            <svg width={width} height={height} className='svg overview' style={{position:"absolute", left:margin.left}}>
                <HypoInfo status={status} />
                <HypoStatus status={status} />
                <Models {...this.state}/>
            </svg>  
        </div>
        <Header changeTHR={this.changeTHR} />
        </div>
    }
}
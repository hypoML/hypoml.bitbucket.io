import * as React from 'react';
import {Select, Button} from "antd";
import {TitleView} from 'const';
import './index.css';

const {Option} = Select
let {width, height} = TitleView

const style = {
    backgroundColor: 'black',
    color: 'white',
    width,
    height,
    padding: '10px 30px'
}

const darkSelectStyle = {
    backgroundColor: 'black', color:"white"
}

export interface Props{
    filename:string,
    changeResults:(filename:string)=>void
}


export default class InfoPanel extends React.Component<Props, {}>{
    private dataset:string; concept:string
    constructor(props:Props){
        super(props)
        this.dataset=''
        this.concept = ''
        this.changeResults=this.changeResults.bind(this)
        this.changeConcept=this.changeConcept.bind(this)
        this.changeData=this.changeData.bind(this)
    }
    changeResults(e:any){
        let {dataset, concept}=this
        e.preventDefault();
        this.props.changeResults(`${dataset}_${concept}.json`)
    }
    changeConcept(concept:any){
        this.concept=concept
    }
    changeData(dataset:any){
        this.dataset=dataset
    }
    render(){
        let {filename}=this.props
        let [dataset, concept] = filename.replace('.json','').split('_')
        this.dataset = dataset
        this.concept = concept
        let featureSelector = <Select defaultValue={concept} size="small" style={darkSelectStyle}
        onChange={this.changeConcept}
        >
            {
            [
                '1.Rotating',
                '2.RandomScaling',
                '3.Scaling_RotationCorrected',
                '4.AverageIntensity',
                '5.IntensityScaling',
                '6.RandomClassNumber',
                '7.ScaledClassNumber',
                '8.RotatingwithBlack',
                "HOG", 
            ].map(d=>{
                return <Option value={d} style={darkSelectStyle}>{d}</Option>
            })
            }
            {/* <Option value="HOG" style={darkSelectStyle}>HOG</Option>
            <Option value="Rotating" style={darkSelectStyle}>Rotating</Option>
            <Option value="Scaling" style={darkSelectStyle}>Scaling</Option>
            <Option value="ScaledandRotated" style={darkSelectStyle}>ScaledandRotated</Option> */}
        </Select>

        let datasetSelector = <Select 
        defaultValue={dataset} size="small" 
        style={darkSelectStyle}
        onChange={this.changeData}
        >
            
            <Option value="fashion" style={darkSelectStyle}>fashion</Option>
            <Option value="cifar10" style={darkSelectStyle}>cifar10</Option>
        </Select>

        return <div className='InfoPanel' style={style}>
            <img
                className='title logo' 
                src='favicon.ico' height={style.height/2}/>
            <div 
                className='title'
                style={{fontSize: style.height/2*0.8+'px', lineHeight:style.height/2+'px', margin: "0px 15px"}}
                > 
                HypoML
            </div>

            <div className='selector dark' style={{width: 'auto', height: style.height}}>
                { featureSelector } 
                { datasetSelector } 
                <Button htmlType="submit" shape="circle" icon="caret-right" onClick={this.changeResults}/>
            </div>

            </div>
    }
}
import * as d3 from 'd3'
import { Painter } from 'components/Painter'

import { ViewParams, CompareData, Results, Label, Metric, ModelData, OneModel, Ratio } from 'types'
import { curveMaker, pairedT, getColor, get95conf, getText } from 'helper'
import { LABELS } from 'const'
import PGlyphPainter from './PGlyph'

export interface Data {
    modelData:ModelData,
    compareData: CompareData,
    thr:number
}

export interface Params extends Data {
    modelView: ViewParams,
    pView: ViewParams
 }


export default class OverViewPainter implements Painter<Data, ViewParams>{
    private params: Params; r: number = 8
    constructor(params: Params) {
        this.params = params
    }
    update(newParams: any) {
        this.params = { ...this.params, ...newParams }
        return this
    }
    

    // ************
    // view controllers
    // *************
    toggleDetails(d:OneModel, xScale:d3.ScaleLinear<number, number>, yScale:d3.ScalePoint<string>){
        let details = d3.select('g.details')

        if (d.hasDetails){
            details.selectAll(`circle.detail.R${d.name}`)
            .remove()

            d.hasDetails = false
        }else{
            details.selectAll(`circle.detail.R${d.name}`)
            .data(d.arr)
            .join(
                enter=>enter.append('circle')
                .attr('class', (_,i)=>`detail R${d.name} label_${i}`)
                .attr('cx', d=>xScale(d))
                .attr('cy', yScale(d.name)||0),
                
                update=>update
                .attr('cx', d=>xScale(d))
                .attr('cy', yScale(d.name)||0),

                exit=>exit.remove()
            )
            .attr('r', this.r*0.8)
            .attr('fill', (d,i)=>getColor(i))

            d.hasDetails = true
        } 

        
    }
    drawDetailLinks(models:ModelData, xScale:d3.ScaleLinear<number, number>, yScale:d3.ScalePoint<string>){
        let detailLinks = d3.select('g.detailLinks')

        let detailedModels = models.filter(d=>d.hasDetails)
        // if (detailedModels.length<2) return

        let d3curve = d3.line()
        .curve(d3.curveCatmullRom.alpha(0.5))

        detailLinks.selectAll(`path.detailLink`)
            .data(LABELS)
            .join(
                enter=>enter.append('path')
                .attr('class', 'detailLink')
                .attr('d', (_, i)=>{
                    let linkdata = detailedModels.map(m=>{return [xScale(m.arr[i]), yScale(m.name)||0] as [number, number]})
                    return d3curve(linkdata)
                }),

                update => update
                .attr('d', (_, i)=>{
                    let linkdata = detailedModels.map(m=>{return [xScale(m.arr[i]), yScale(m.name)||0] as [number, number]})
                    return d3curve(linkdata)
                }),

                exit=> exit
                .remove()
            )
            .attr('stroke', (_, i)=>getColor(i))
            .attr('fill', 'none')
            .attr('stroke-width', 2)

    }

    // *********
    // render
    // *********
    render(selector: d3.Selection<SVGElement, any, any, any>) {
        selector.selectAll('*').remove()

        let { modelView, pView, modelData, compareData } = this.params

        // ***********
        // draw model points
        // ***********



        // Set the ranges
        const xmin = Math.min(...modelData.map(d => Math.min(...d.arr))) || 0
        const xmax = Math.max(...modelData.map(d => Math.max(...d.arr))) || 1

        let accScale = d3.scaleLinear()
            .range([0, modelView.width-modelView.margin.left-modelView.margin.right])
            .domain([xmin, xmax])
        // .domain([0.7,1])


        let yScale = d3.scalePoint()
            .range([0, modelView.height])
            .domain(modelData.map(d => d.name))

       


        // **************************
        // models 
        // **************************


        let modelGroup = selector.append('g')
            .attr('class', 'models')
            .attr("transform", `translate(${pView.width+ modelView.margin.left},${pView.margin.top})`)

        // // append x axis
        // modelGroup.append("g")
        // .attr("transform", `translate(0,${height})`)
        // .call(d3.axisBottom(accScale))

        let std = modelGroup.append('g')
            .attr('class', 'std')

        std.selectAll('line.std')
            .data(modelData)
            .join(
                enter => enter.append('line')
                    .attr('x1', (d) => accScale(d.avg - d.err))
                    .attr('x2', (d) => accScale(d.avg + d.err))
                    .attr('y1', (d) => yScale(d.name) || 0)
                    .attr('y2', (d) => yScale(d.name) || 0),

                update => update
                    .attr('x1', (d) => accScale(d.avg - d.err))
                    .attr('x2', (d) => accScale(d.avg + d.err))
                    .attr('y1', (d) => yScale(d.name) || 0)
                    .attr('y2', (d) => yScale(d.name) || 0),

                exit=>exit.remove()
            )
            .style('stroke', "black")





        let points = modelGroup.append('g')
            .attr('class', 'points')

        d3.select('g.points')
            .append('g')
            .attr('class', 'details')

        d3.select('g.points')
            .append('g')
            .attr('class', 'detailLinks')

        this.drawDetailLinks(modelData, accScale, yScale)

        points.selectAll('circle.point')
            .data(modelData)
            .join(
                enter => enter.append("circle")
                    .attr('cx', (d) => accScale(d.avg))
                    .attr('cy', (d) => yScale(d.name) || 0),

                update => update.attr('cx', (d) => accScale(d.avg))
                    .attr('cy', (d) => yScale(d.name) || 0),

                exit => exit.remove()
            )
            .attr('class', 'point')
            .attr('r', this.r)
            .attr("stroke", "black")
            .attr("fill", "white")
            .on('click', (d)=>{
                this.toggleDetails(d, accScale, yScale)
                this.drawDetailLinks(modelData, accScale, yScale)
            })



        let labels = modelGroup.append('g')
            .attr('class', 'labels')
        
        labels.selectAll('.image')
            .data(modelData)
            .join(
                enter => enter.append("svg:image")
                    .attr('class', 'image')
                    .attr("xlink:href", (d)=>`../data/icon/${getText(d.name)}.png`)
                    .attr("x",10)
                    .attr('y', (d) => (yScale(d.name) || 0)-2*this.r)
                    .attr("width", "40")
                    .attr("height", "40"),

                // update => update
                //     .attr('x', 0)
                //     .attr('y', (d) => (yScale(d.name) || 0)-2*this.r)
                //     .text(d => getText(d.name)),

                exit => exit.remove()
            )
            .attr('text-anchor', 'start')

        labels.selectAll('text.point')
            .data(modelData)
            .join(
                enter => enter.append("text")
                    .attr('class', 'label')
                    .attr('x', 0)
                    // .attr('x',0)
                    .attr('y', (d) => (yScale(d.name) || 0)-2*this.r)
                    .text(d => getText(d.name)),

                update => update
                    .attr('x', 0)
                    .attr('y', (d) => (yScale(d.name) || 0)-2*this.r)
                    .text(d => getText(d.name)),

                exit => exit.remove()
            )
            .attr('text-anchor', 'start')

        let pointValue = modelGroup.append('g')
            .attr('class', 'values')

        pointValue.selectAll('text.value')
            .data(modelData)
            .join(
                enter => enter.append("text")
                    .attr('class', 'value')
                    .attr('x', (d) => accScale(d.avg) - this.r)
                    .attr('y', (d) => (yScale(d.name) || 0) - 2 * this.r)
                    .text((d) => d.avg.toFixed(4)),

                update => update
                    .attr('x', (d) => accScale(d.avg) - this.r)
                    .attr('y', (d) => (yScale(d.name) || 0) - 2 * this.r)
                    .text((d) => d.avg.toFixed(4)),

                exit => exit.remove()
            )


        // *****************
        // draw p links
        // *******************
        let pValueScale = d3.scaleLinear()
        .domain([Math.min(...compareData.map(d=>d.p)), Math.max(...compareData.map(d=>d.p))])
        .range([2, 0.8])


        let yScaleP = d3.scaleLinear()
            .range([0, pView.height])
            .domain([0, compareData.length - 1])

        let pGroup = selector.append('g')
            .attr('class', 'pValues')
            .attr("transform", `translate(${pView.margin.left},${pView.margin.top})`)
        
        let pGlyphParams = {
                width: yScaleP(1)*1.5,
                height: yScaleP(1)*0.5,
                margin:{top:1, bottom:1, left:1, right:1}
            }


        pGroup.selectAll('path.pValue')
            .data(compareData)
            .join(
                enter => enter.append('path')
                    .attr('class', 'pValue')
                    .attr('d', (d, i) => {return `M${pView.width} ${yScale(d.from) || 0} L${pGlyphParams.width*0.5} ${yScaleP(i) || 0}
                            L${pView.width} ${yScale(d.to) || 0}`
                    }),

                update=>update
                .attr('d', (d, i) => {return `M${pView.width} ${yScale(d.from) || 0} L${pGlyphParams.width*0.5} ${yScaleP(i) || 0}
                            L${pView.width} ${yScale(d.to) || 0}`
                    }),

                exit=>exit.remove()
            )
            .attr('fill', 'none')
            .attr('stroke', 'black')
            .attr("stroke-dasharray", (d) => `8, ${d.p > this.params.thr ? 4 : 0}`)
            .attr('stroke-width', d => {
                return pValueScale(d.p)})

        // ********************************
        // draw the p value glyph
        // ********************************

        // pGroup.selectAll('g.pGlyph')
        //     .data(compareData)
        //     .join(
        //         // enter=>enter.append('g')
        //         // .attr('class', 'pGlyph')
        //         // .attr('transform', (d,i)=>`translate(${0-pGlyphParams.width/2}, ${yScaleP(i)-pGlyphParams.height*0.8})`)
        //         // .call(
        //         //     selector => new PGlyphPainter(pGlyphParams)
        //         //     .render(selector)
        //         // ),
        //         // exit=>exit.remove()
        //         enter=>enter.append('circle')
        //         .attr('cy', (d,i)=>yScaleP(i)-pGlyphParams.height*0.8+15*d.p)
        //         .attr('cx', (d)=>15*d.p)
        //         .attr('r', (d)=>15*Math.sqrt(d.p))
        //         .attr('stroke', 'black')
        //     )
            
            pGroup.selectAll('g.pGlyph')
            .data(compareData)
            .join(
                enter=>enter.append('g')
                .attr('class', 'pGlyph')
                .attr('transform', (d,i)=>`translate(${0-pGlyphParams.width/2}, ${yScaleP(i)-pGlyphParams.height*0.8})`)
                .call(
                    selector => new PGlyphPainter(pGlyphParams)
                    .render(selector)
                ),
                exit=>exit.remove()
                // enter=>enter.append('circle')
                // .attr('cy', (d,i)=>yScaleP(i)-pGlyphParams.height*0.8+15*d.p)
                // .attr('cx', (d)=>15*d.p)
                // .attr('r', (d)=>15)
                // .attr('stroke', 'black')
                // .attr('fill', 'none')
            )
            


        return this
    }
}
import * as d3 from 'd3'
import { Painter} from 'components/Painter'
import {percentile, NormalDist, getText} from 'helper';
import {ViewParams, CompareData} from 'types'
import { drag, EnterElement, BaseType } from 'd3';


type Data = CompareData[0]

interface Params extends Data, ViewParams{}

export default class  PGlyphPainter implements Painter<Data, ViewParams>{
    static m = 0; static std = 1
    static arr = NormalDist(PGlyphPainter.m, PGlyphPainter.std)
    private viewParams: ViewParams;
    constructor(viewParams: ViewParams) {
        this.viewParams = viewParams
    }
    update(newParams: Partial<ViewParams>) {
        this.viewParams = { ...this.viewParams, ...newParams }
        return this
    }
    render(selector:d3.Selection<SVGElement, Data, any, any>|d3.Selection<SVGGElement, Data, any, any>) {
        let {width, height, margin} = this.viewParams
        let {m, std} = PGlyphPainter
        

        width = width - margin.left-margin.right
        height = height-margin.top-margin.bottom

        let g = selector

        var xScale = d3.scaleLinear()
            .domain([m-4*std, m+4*std])
            .range([0, width]);

        var yScale = d3.scaleLinear()
        .domain([0, Math.max(...PGlyphPainter.arr.map(d=>d[1]))])
        .range([height, 0]);

        var line = d3.line()
            .x((d)=>xScale(d[0]))
            .y((d)=>yScale(d[1]))

        

        g.append('path')
        .datum(PGlyphPainter.arr)
        .attr("class", "dist")
        .attr("d", line)
        .style("fill", "cornflowerblue")
        
        
        g.append("clipPath")       // define a clip path
        .attr("id", (d,i)=>`${d.from}_${d.to}`) // give the clipPath an ID
        .append('rect')
        .attr('class', 'z')
        .attr('x', d=>xScale(percentile(d.p, m, std)))
        .attr('width', d=>width-xScale(percentile(d.p, m, std)))
        .attr('y', 0)
        .attr('height', height)
        .attr('fill','orange')
        .style("opacity", "0.8");

        g.append('path')
        .attr("clip-path", (d,i)=>`url(#${d.from}_${d.to})`)
        .datum(PGlyphPainter.arr)
        .attr("class", "dist")
        .attr("d", line)
        .style("fill", "orange")

        let text = g.append('text')
        .attr('class', 'pValue')
        

        text.append('tspan')
        .text(d => d.p.toFixed(3))
        .attr("text-anchor", 'middle')
        .attr('x', width/2)
        // .attr('x', d => (ratio.p+ratio.model)*width)
        .attr('y', 1.4*height)

        

        text.append('tspan')
        .text(d => {
            return `${getText(d.from)}${d.dir=='+'?'>':'<'}${getText(d.to)}`
        })
        .attr('x', width/2)
        // .attr('x', d => (ratio.p+ratio.model)*width)
        .attr('y', 15+1.4*height)
        .attr("text-anchor", 'middle')



        return this
    }
}
import React from 'react';
import * as d3 from 'd3';
import ModelPainter, {Params} from '../Models/ModelPainter';
import {HypoTextView, modelView, pView} from 'const'
import {CompareData, ModelData} from 'types';

interface Props {
    modelData: ModelData,
    compareData: CompareData,
    thr:number
}


export default class Overview extends React.Component<Props, {}>{
    private painter:ModelPainter
    constructor(props:any){
        super(props)
        let params = {
            modelView,
            pView,
            ...this.props,
        }
        this.painter = new ModelPainter(params)
    }
    painterUpdate(newParams: Partial<Params>){
        this.painter.update(newParams)
        .render(d3.select('g.modelPainter'))
    }
    // componentDidMount(){
    //     this.getJSON('data/results.json')

    // }
    shouldComponentUpdate(nextProps:Props){
        this.painterUpdate(nextProps)
        return false
    }
    render(){
        return <g className='modelPainter' transform={`translate(${HypoTextView.width}, ${HypoTextView.height})`}/>
    }
}
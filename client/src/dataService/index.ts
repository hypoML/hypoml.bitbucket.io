import axios, { AxiosResponse } from 'axios';
import {AccuracyData, Results} from 'types'

function checkResponse<T>(response: AxiosResponse<T>, fallback: T): T {
    if (response.status === 200)
      return response.data;
    console.error(`Data fetching error: ${response.status}`);
    console.error(response);
    throw response;
}


export async function fetchCSV(filename:string):Promise<AccuracyData>{
    const response = await axios.get(filename);
    if (response.status === 200){
      let res = response.data
      return res.split('\n').map((line:string)=>{
        let v =line.split(',')
        return [v[0], parseFloat(v[1])||Math.random()*0.035+0.81]
      })
    }
    console.error(`Data fetching error: ${response.status}`);
    throw response;
}


export async function fetchJSON(filename:string):Promise<Results>{
  const response = await axios.get(filename);
  return checkResponse(response, null);
}